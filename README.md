# Dockerized Gatling for Load Testing
This project contains a dockerized gatling, which is used in gatlingxuc project.

## Build
Please set the TARGET_VERSION to the version of gatling you want to build.

## Development
For each new version of gatling a new branch should be created, where Dockerfile  
is developed so that the image building, and container runtime then have no issues  
with the new gatling version.  
Once this is achieved, the branch should be merged and a tag with the gatling  
version should be created.
